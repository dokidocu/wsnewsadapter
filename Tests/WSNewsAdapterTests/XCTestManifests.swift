import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(WSNewsAdapterTests.allTests),
    ]
}
#endif
