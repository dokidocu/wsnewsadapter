import XCTest
import HLBaseAdapterKit
@testable import WSNewsAdapter

private let timeout = 4.0

final class WSNewsRequestTests: XCTestCase {

    var urlSession: URLSession!

    lazy var restConfiguration = RestConfiguration(scheme: "https", host: "newsapi.org", version: "v2")
    lazy var newsConfiguration = WSNewsConfiguration(apiKey: "f4f7841274b04f3db5277cc9e8a535e0")
    lazy var wsNewsAPI = WSNewsAPI(newsConfiguration: newsConfiguration, restConfiguration: restConfiguration)

    override func setUp() {
        urlSession = URLSession.shared
    }

    override func tearDown() {
        urlSession = nil
    }

    func testGetArticlesMissingParameters() {
        let expect = expectation(description: "Received data")

        let request = WSNewsRequest.Articles(query: nil,
                                             qInTitle: nil,
                                             sources: nil,
                                             domains: nil,
                                             excludeDomains: nil,
                                             from: nil,
                                             to: nil,
                                             language: nil,
                                             sortBy: nil,
                                             page: 20,
                                             pageSize: 0)

        wsNewsAPI.getArticles(request: request, urlSession: urlSession) { result in
            if case .failure(let val) = result,
               case WSNewsError.missingParameters(_) = val
            {
                expect.fulfill()
            }
        }

        wait(for: [expect], timeout: timeout)
    }

    func testGetArticles() {
        let expect = expectation(description: "Received data")

        let request = WSNewsRequest.Articles(query: nil,
                                             qInTitle: nil,
                                             sources: nil,
                                             domains: "wsj.com",
                                             excludeDomains: nil,
                                             from: nil,
                                             to: nil,
                                             language: nil,
                                             sortBy: nil,
                                             page: 20,
                                             pageSize: 1)

        wsNewsAPI.getArticles(request: request, urlSession: urlSession) { result in
            if case .success(let response) = result,
               false == response.articles.isEmpty
            {
                expect.fulfill()
            }
        }

        wait(for: [expect], timeout: timeout)
    }

    func testGetHeadlinesMissingParameters() {
        let expect = expectation(description: "Received data")

        let request = WSNewsRequest.Headlines(category: nil,
                                              country: nil,
                                              sources: nil,
                                              query: nil,
                                              page: 1)

        wsNewsAPI.getHeadlines(request: request, urlSession: urlSession) { result in
            if case .failure(let val) = result,
               case WSNewsError.missingParameters(_) = val
            {
                expect.fulfill()
            }
        }

        wait(for: [expect], timeout: timeout)
    }

    func testGetHeadlines() {
        let expect = expectation(description: "Received data")

        let request = WSNewsRequest.Headlines(category: .general,
                                              country: nil,
                                              sources: nil,
                                              query: nil,
                                              page: 1)

        wsNewsAPI.getHeadlines(request: request, urlSession: urlSession) { result in
            if case .success(let response) = result,
               false == response.articles.isEmpty
            {
                expect.fulfill()
            }
        }

        wait(for: [expect], timeout: timeout)
    }

    func testGetSources() {
        let expect = expectation(description: "Received data")

        let request = WSNewsRequest.Sources(language: nil,
                                            country: nil,
                                            category: nil)

        wsNewsAPI.getSources(request: request, urlSession: urlSession) { result in
            if case .success(let response) = result,
               false == response.sources.isEmpty
            {
                expect.fulfill()
            }
        }

        wait(for: [expect], timeout: timeout)
    }

    func testGetIncorrectArticles() {
        let expect = expectation(description: "Should not receive data")

        let request = WSNewsRequest.Articles(query: nil,
                                             qInTitle: nil,
                                             sources: "WTF wrong",
                                             domains: nil,
                                             excludeDomains: nil,
                                             from: nil,
                                             to: nil,
                                             language: nil,
                                             sortBy: nil,
                                             page: 20,
                                             pageSize: 1)

        wsNewsAPI.getArticles(request: request, urlSession: urlSession) { result in
            if case .failure(_) = result
            {
                expect.fulfill()
            }
        }

        wait(for: [expect], timeout: timeout)
    }
}
