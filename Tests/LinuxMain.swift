import XCTest

import WSNewsAdapterTests

var tests = [XCTestCaseEntry]()
tests += WSNewsAdapterTests.allTests()
XCTMain(tests)
