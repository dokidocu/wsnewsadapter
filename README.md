# WSNewsAdapter

Adapter for https://newsapi.org/

## Supporting Version
===> v2 : 2020-01-17

## How to use it (example)

1. Initialisation

```swift
  let newsConfiguration = WSNewsConfiguration(apiKey: "_KEY_")
  let newsAPI = WSNewsAPI(newsConfiguration: newsConfiguration)
```
2. Get Articles

```swift
  let request = WSNewsRequest.Articles(query: nil,
                                      qInTitle: nil,
                                      sources: nil,
                                      domains: nil,
                                      excludeDomains: nil,
                                      from: nil,
                                      to: nil,
                                      language: nil,
                                      sortBy: nil,
                                      page: 20,
                                      pageSize: 0)
                                      
  wsNewsAPI.getArticlesDataTask(request: request,
                                urlSession: URLSession.shared)
  { (sourcesResult, error) in
    // Handle callback
  }?.resume
```

2. Get Headlines

```swift
  let request = WSNewsRequest.Headlines(category: nil,
                                        country: nil,
                                        sources: nil,
                                        query: nil,
                                        page: 20,
                                        pageSize: 0)
  
  wsNewsAPI.getHeadlinesDataTask(request: request, urlSession: urlSession)
  { (headlineResult, error) in
    // Handle callback
  }?.resume()
```

3. Get Sources

```swift
  let request = WSNewsRequest.Sources(language: nil, country: nil, category: nil)
  
  wsNewsAPI.getSourcesDataTask(request: request, urlSession: urlSession)
  { (sourcesResult, error) in
    // Handle callback
  }?.resume()
```
