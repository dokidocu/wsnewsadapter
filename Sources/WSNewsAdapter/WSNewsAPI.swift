//
//  File.swift
//
//
//  Created by Henri LA on 09.01.20.
//
import HLBaseAdapterKit
import Foundation

private struct WSNewsPath {
  private init(){}
  
  static let getArticles = "/everything"
  static let getHeadlines = "/top-headlines"
  static let getSources = "/sources"
}

public struct WSNewsAPI: WSNewsAPIService {
  
    // MARK: Configurations
  
    public let restConfiguration: RestConfiguration
    public let newsConfiguration: WSNewsConfiguration
  
    // MARK: Init
  
    public init(newsConfiguration: WSNewsConfiguration,
              restConfiguration: RestConfiguration = RestConfiguration(scheme: "https",
                                                                       host: "newsapi.org",
                                                                       version: "v2"))
    {
        self.newsConfiguration = newsConfiguration
        self.restConfiguration = restConfiguration
    }
  
    // MARK: WSNewsAPIService implementation

    public func getArticlesDataTask(request: WSNewsRequest.Articles,
                                    urlSession: URLSession,
                                    completion: @escaping WSTopHeadlineResult) -> URLSessionDataTask?
    {
        guard request.isValid else {
            completion(.failure(request.error))
            return nil
        }

        guard let url = restConfiguration.url(path: WSNewsPath.getArticles,
                                              queryItems: request.queryItems) else
        {
            completion(.failure(.invalidURL("\(WSNewsPath.getArticles): \(request.queryItems)")))
            return nil
        }

        let result = urlSession.dataTask(with: url.urlRequest(newsConfiguration: newsConfiguration))
        {
            (data, response, error) in

            if let error = error {
                completion(.failure(.dataTaskError(error)))
            } else {
                guard let data = data else {
                    completion(.failure(.noData))
                    return
                }

                let jsonDecoder = JSONDecoder()

                if let okResult = try?  jsonDecoder.decode(WSNewsTopHeadlineResponse.self, from: data) {
                    completion(.success(okResult))
                }
                else if let backendError = try?  jsonDecoder.decode(WSNewsBackendError.self, from: data) {
                    completion(.failure(.backendError(backendError)))
                }
                else {
                    completion(.failure(.parsingFailed))
                }

            }
        }

        return result
    }

    public func getArticles(request: WSNewsRequest.Articles,
                            urlSession: URLSession,
                            completion: @escaping WSTopHeadlineResult)
    {
        getArticlesDataTask(request: request,
                            urlSession: urlSession,
                            completion: completion)?.resume()
    }

    public func getHeadlinesDataTask(request: WSNewsRequest.Headlines,
                              urlSession: URLSession,
                              completion: @escaping WSTopHeadlineResult) -> URLSessionDataTask?
    {
        guard request.isValid else {
            completion(.failure(request.error))
            return nil
        }

        guard let url = restConfiguration.url(path: WSNewsPath.getHeadlines,
                                              queryItems: request.queryItems) else
        {
            completion(.failure(.invalidURL("\(WSNewsPath.getHeadlines): \(request.queryItems)")))
            return nil
        }

        let result = urlSession.dataTask(with: url.urlRequest(newsConfiguration: newsConfiguration))
        {
            (data, response, error) in

            if let error = error {
                completion(.failure(.dataTaskError(error)))
            } else {
                guard let data = data else {
                    completion(.failure(.noData))
                    return
                }

                let jsonDecoder = JSONDecoder()

                if let okResult = try?  jsonDecoder.decode(WSNewsTopHeadlineResponse.self, from: data) {
                    completion(.success(okResult))
                }
                else if let backendError = try?  jsonDecoder.decode(WSNewsBackendError.self, from: data) {
                    completion(.failure(.backendError(backendError)))
                }
                else {
                    completion(.failure(.parsingFailed))
                }

            }
        }

        return result
    }

    public func getHeadlines(request: WSNewsRequest.Headlines,
                      urlSession: URLSession,
                      completion: @escaping WSTopHeadlineResult)
    {
        getHeadlinesDataTask(request: request,
                             urlSession: urlSession,
                             completion: completion)?.resume()
    }

    public func getSourcesDataTask(request: WSNewsRequest.Sources,
                                   urlSession: URLSession,
                                   completion: @escaping WSNewsSourcesResult) -> URLSessionDataTask?
    {
        guard let url = restConfiguration.url(path: WSNewsPath.getSources,
                                              queryItems: request.queryItems) else
        {
            completion(.failure(.invalidURL("\(WSNewsPath.getSources): \(request.queryItems)")))
            return nil
        }

        let result = urlSession.dataTask(with: url.urlRequest(newsConfiguration: newsConfiguration))
        {
            (data, response, error) in

            if let error = error {
                completion(.failure(.dataTaskError(error)))
            } else {
                guard let data = data else {
                    completion(.failure(.noData))
                    return
                }

                let jsonDecoder = JSONDecoder()

                if let okResult = try?  jsonDecoder.decode(WSNewsSourcesResponse.self, from: data) {
                    completion(.success(okResult))
                }
                else if let backendError = try?  jsonDecoder.decode(WSNewsBackendError.self, from: data) {
                    completion(.failure(.backendError(backendError)))
                }
                else {
                    completion(.failure(.parsingFailed))
                }

            }
        }

        return result
    }

    public func getSources(request: WSNewsRequest.Sources,
                           urlSession: URLSession,
                           completion: @escaping WSNewsSourcesResult)
    {
        getSourcesDataTask(request: request,
                           urlSession: urlSession,
                           completion: completion)?.resume()
    }
  
}


private extension URLResponse {

    /// The response’s HTTP status code.
    var code: Int {
        guard let httpResponse = self as? HTTPURLResponse else {
          return -1
        }

        return httpResponse.statusCode
    }

}
