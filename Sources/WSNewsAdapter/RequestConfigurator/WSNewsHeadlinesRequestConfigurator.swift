//
//  File.swift
//  
//
//  Created by Henri LA on 25.09.20.
//

import Foundation

public class WSNewsHeadlinesRequestConfigurator {

    // MARK: Parameters

    public var category: WSNewsCategory? {
        didSet { resetPage() }
    }

    public var country: WSNewsCountry? {
        didSet { resetPage() }
    }

    public var sources: String? {
        didSet { resetPage() }
    }

    public var query: String? {
        didSet { resetPage() }
    }
    
    private(set) public var page: Int = 0
    private(set) public var pageSize: Int

    // MARK: Init

    public init(pageSize: Int = 20) {
        self.pageSize = pageSize
    }

    // MARK: Private function

    private func resetPage() {
        page = 0
    }

    // MARK: Public function

    public func nextPage() -> WSNewsRequest.Headlines {
        page += 1
        let nextRequest =  WSNewsRequest.Headlines(category: category,
                                                   country: country,
                                                   sources: sources,
                                                   query: query,
                                                   page: page,
                                                   pageSize: pageSize)
        return nextRequest
    }

}
