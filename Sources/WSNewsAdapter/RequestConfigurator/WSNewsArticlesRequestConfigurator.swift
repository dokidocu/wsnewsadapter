//
//  File.swift
//  
//
//  Created by Henri LA on 25.09.20.
//

import Foundation

public class WSNewsArticlesRequestConfigurator {

    // MARK: Parameters

    public var query: String? {
        didSet { resetPage() }
    }
    public var qInTitle: String? {
        didSet { resetPage() }
    }

    public var sources: String? {
        didSet { resetPage() }
    }

    public var domains: String? {
        didSet { resetPage() }
    }

    public var excludeDomains: String? {
        didSet { resetPage() }
    }

    public var from: Date? {
        didSet { resetPage() }
    }

    public var to: Date? {
        didSet { resetPage() }
    }

    public var language: WSNewsLanguage? {
        didSet { resetPage() }
    }

    public var sortBy: WSNewsSortBy? {
        didSet { resetPage() }
    }

    private(set) public var page: Int = 0
    private(set) public var pageSize: Int

    // MARK: Init

    public init(pageSize: Int = 20){
        self.pageSize = pageSize
    }

    // MARK: Private function

    private func resetPage() {
        page = 0
    }

    // MARK: Public function

    public func nextPage() -> WSNewsRequest.Articles {
        page += 1
        let nextRequest =  WSNewsRequest.Articles(query: query,
                                                  qInTitle: qInTitle,
                                                  sources: sources,
                                                  domains: domains,
                                                  excludeDomains: excludeDomains,
                                                  from: from,
                                                  to: to,
                                                  language: language,
                                                  sortBy: sortBy,
                                                  page: page,
                                                  pageSize: pageSize)
        return nextRequest
    }
    
}
