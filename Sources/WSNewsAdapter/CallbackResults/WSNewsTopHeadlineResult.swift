//
//  WSTopHeadlineResult.swift
//  HLKit
//
//  Created by Henri LA on 26.10.19.
//

import Foundation

public typealias WSTopHeadlineResult = (Result<WSNewsTopHeadlineResponse, WSNewsError>) -> ()

public struct WSNewsTopHeadlineResponse: Codable {
  /// If the request was successful or not. Options: ok, error.
  /// In the case of error a code and message property will be populated.
  public let status: String?
  /// The total number of results available for your request.
  /// Only a limited number are shown at a time though, so use the page parameter
  /// in your requests to page through them.
  public let totalResults: Int
  /// The results of the request.
  public let articles: [WSNewsArticle]
  
}
