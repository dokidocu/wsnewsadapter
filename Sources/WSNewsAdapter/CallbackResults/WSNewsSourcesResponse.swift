//
//  WSNewsSourcesResult.swift
//  HLKit
//
//  Created by Henri LA on 26.10.19.
//

import Foundation

public typealias WSNewsSourcesResult = (Result<WSNewsSourcesResponse, WSNewsError>) -> ()

public struct WSNewsSourcesResponse: Codable {
  
  /// If the request was successful or not. Options: ok, error.
  /// In the case of error a code and message property will be populated.
  public let status: String?
  /// The results of the request.
  public let sources: [WSNewsSource]
  
}
