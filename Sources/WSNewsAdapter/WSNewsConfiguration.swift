//
//  WSNewsConfiguration.swift
//  HLKit
//
//  Created by Henri LA on 26.10.19.
//

import Foundation
import HLBaseAdapterKit

/// Configuration needed to use the news Adapter
public struct WSNewsConfiguration {
  
  /// API key
  public let apiKey: String
  
  public init(apiKey: String) {
    if apiKey == "" {
      fatalError("API Key for News should not be empty")
    }
    
    if apiKey.count < 10 {
      fatalError("Invalid API Key")
    }
    self.apiKey = apiKey
  }
  
}
