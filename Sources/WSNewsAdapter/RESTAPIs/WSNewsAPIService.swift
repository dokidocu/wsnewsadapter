//
//  File.swift
//  
//
//  Created by Henri LA on 17.01.20.
//

import Foundation

public protocol WSNewsAPIService {

    func getArticlesDataTask(request: WSNewsRequest.Articles,
                             urlSession: URLSession,
                             completion: @escaping WSTopHeadlineResult ) -> URLSessionDataTask?

    func getArticles(request: WSNewsRequest.Articles,
                     urlSession: URLSession,
                     completion: @escaping WSTopHeadlineResult)

    func getHeadlinesDataTask(request: WSNewsRequest.Headlines,
                              urlSession: URLSession,
                              completion: @escaping WSTopHeadlineResult) -> URLSessionDataTask?

    func getHeadlines(request: WSNewsRequest.Headlines,
                      urlSession: URLSession,
                      completion: @escaping WSTopHeadlineResult)

    func getSourcesDataTask(request: WSNewsRequest.Sources,
                            urlSession: URLSession,
                            completion: @escaping WSNewsSourcesResult) -> URLSessionDataTask?

    func getSources(request: WSNewsRequest.Sources,
                    urlSession: URLSession,
                    completion: @escaping WSNewsSourcesResult)

}
