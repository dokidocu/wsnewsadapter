//
//  File.swift
//  
//
//  Created by Henri LA on 10.01.20.
//

import Foundation

extension URL {
  
  func urlRequest(newsConfiguration: WSNewsConfiguration) -> URLRequest {
    var request = URLRequest(url: self)
    
    request.setValue("Bearer \(newsConfiguration.apiKey)", forHTTPHeaderField: "Authorization")
    
    return request
  }
  
}
