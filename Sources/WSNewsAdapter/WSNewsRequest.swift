//
//  File.swift
//  
//
//  Created by Henri LA on 10.01.20.
//

import Foundation
import HLBaseAdapterKit

public struct WSNewsRequest {
    private init(){}
}

// MARK: Articles

public extension WSNewsRequest {

    struct Articles {

        let queryItems: [URLQueryItem]
        let error: WSNewsError

        var isValid: Bool {
            let result: Bool
            if case WSNewsError.none = error {
                result = true
            } else {
                result = false
            }
            return result
        }

        public init(query: String?,
                    qInTitle: String?,
                    sources: String?,
                    domains: String?,
                    excludeDomains: String?,
                    from: Date?,
                    to: Date?,
                    language: WSNewsLanguage?,
                    sortBy: WSNewsSortBy?,
                    page: Int,
                    pageSize: Int = 20)
        {
            var queryItems = [URLQueryItem]()
            var isValid = false

            // Page + PageSize
            queryItems.append(URLQueryItem(name: "page",
                                           value: "\(page)"))
            queryItems.append(URLQueryItem(name: "pageSize",
                                           value: "\(pageSize)"))

            // Query + need to be encoded
            if let query = query,
            let encoded = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            {
                queryItems.append(URLQueryItem(name: "q",
                                               value: encoded))
                isValid = true
            }

            // qInTitle + need to be encoded
            if let query = qInTitle,
            let encoded = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            {
                queryItems.append(URLQueryItem(name: "qInTitle",
                                               value: encoded))
                isValid = true
            }

            // Sources
            if let sources = sources {
                queryItems.append(URLQueryItem(name: "sources",
                                               value: sources))
                isValid = true
            }

            // domains
            if let domains = domains {
                queryItems.append(URLQueryItem(name: "domains",
                                               value: domains))
                isValid = true
            }

            // excludeDomains
            if let excludeDomains = excludeDomains {
                queryItems.append(URLQueryItem(name: "excludeDomains",
                                               value: excludeDomains))
            }

            // from + need to be iso8601
            if let from = from {
                queryItems.append(URLQueryItem(name: "from",
                                               value: from.iso8601))
            }

            // to + need to be iso8601
            if let to = to {
                queryItems.append(URLQueryItem(name: "to",
                                               value: to.iso8601))
            }

            // language
            if let language = language {
                queryItems.append(URLQueryItem(name: "language",
                                               value: language.rawValue))
            }

            //sortBy
            if let sortBy = sortBy {
                queryItems.append(URLQueryItem(name: "sortBy",
                                               value: sortBy.rawValue))
            }

            self.queryItems = queryItems
            if isValid {
                error = .none
            } else {
                error = .missingParameters("Required parameters are missing: q, qInTitle, sources, domains.")
            }
        }

    }
}

// MARK: Headlines

public extension WSNewsRequest {

    struct Headlines {

        let queryItems: [URLQueryItem]
        let error: WSNewsError

        var isValid: Bool {
            let result: Bool
            if case WSNewsError.none = error {
                result = true
            } else {
                result = false
            }
            return result
        }

        public init(category: WSNewsCategory?,
                    country: WSNewsCountry?,
                    sources: String?,
                    query: String?,
                    page: Int,
                    pageSize: Int = 20)
        {
            var queryItems = [URLQueryItem]()
            var isValid = false

            // Page + PageSize
            queryItems.append(URLQueryItem(name: "page",
                                           value: "\(page)"))
            queryItems.append(URLQueryItem(name: "pageSize",
                                           value: "\(pageSize)"))

            // Country
            if let country = country {
                queryItems.append(URLQueryItem(name: "country",
                                               value: country.rawValue))
                isValid = true
            }

            // Category
            if let category = category {
                queryItems.append(URLQueryItem(name: "category",
                                               value: category.rawValue))
                isValid = true
            }

            // Sources
            if let sources = sources {
                queryItems.append(URLQueryItem(name: "sources",
                                               value: sources))
                isValid = true
            }

            // Query
            if let query = query {
                queryItems.append(URLQueryItem(name: "q",
                                               value: query))
                isValid = true
            }

            self.queryItems = queryItems
            if isValid {
                error = .none
            } else {
                error = .missingParameters("Required parameters are missing: sources, q, language, country, category.")
            }
        }

    }

}

// MARK: Sources

public extension WSNewsRequest {
 
    struct Sources {

        let queryItems: [URLQueryItem]

        public init(language: WSNewsLanguage?,
                    country: WSNewsCountry?,
                    category: WSNewsCategory?)
        {
            var queryItems = [URLQueryItem]()

            // Language
            if let language = language {
                queryItems.append(URLQueryItem(name: "language",
                                               value: language.rawValue))
            }

            // Country
            if let country = country {
                queryItems.append(URLQueryItem(name: "country",
                                               value: country.rawValue))
            }

            // Category
            if let category = category {
                queryItems.append(URLQueryItem(name: "category",
                                               value: category.rawValue))
            }

            self.queryItems = queryItems
        }
    }
}

// MARK: Private Date extension

private extension Date {

    private static let iso8601Formatter = ISO8601DateFormatter()

    var iso8601: String {
        return Date.iso8601Formatter.string(from: self)
    }
  
}
