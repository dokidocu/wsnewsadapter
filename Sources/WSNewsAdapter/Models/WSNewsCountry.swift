//
//  WSNewsCountry.swift
//  HLKit
//
//  Created by Henri LA on 26.10.19.
//

import Foundation

/// The 2-letter ISO 3166-1 code of the country you want to get headlines for.
public enum WSNewsCountry: String, CaseIterable {
  
  case ae
  case ar
  case at
  case au
  case be
  case bg
  case br
  case ca
  case ch
  case cn
  case co
  case cu
  case cz
  case de
  case eg
  case fr
  case gb
  case gr
  case hk
  case hu
  case id
  case ie
  case il
  case `in`
  case it
  case jp
  case kr
  case lt
  case lv
  case ma
  case mx
  case my
  case ng
  case nl
  case no
  case nz
  case ph
  case pl
  case pt
  case ro
  case rs
  case ru
  case sa
  case se
  case sg
  case si
  case sk
  case th
  case tr
  case tw
  case ua
  case us
  case ve
  case za
  
}
