//
//  WSNewsBackendError.swift
//  HLKit
//
//  Created by Henri LA on 26.10.19.
//

import Foundation

public struct WSNewsBackendError: Codable {
  
  public let status: String
  public let code: String
  public let message: String
  
  public var wsNewsError: WSNewsError {
    return .backendError(self)
  }
}

