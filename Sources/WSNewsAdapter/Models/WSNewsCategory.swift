//
//  WSNewsCategory.swift
//  HLKit
//
//  Created by Henri LA on 26.10.19.
//

import Foundation

/// The category you want to get headlines for.
public enum WSNewsCategory: String, CaseIterable, Codable {
  
  case business
  case entertainment
  case general
  case health
  case science
  case sports
  case technology
  
}
