//
//  WSNewsLanguage.swift
//  HLKit
//
//  Created by Henri LA on 26.10.19.
//

import Foundation

/// The 2-letter ISO-639-1 code of the language you want to get headlines for.
public enum WSNewsLanguage: String, CaseIterable, Codable {
  
  case ar
  case de
  case en
  case es
  case fr
  case he
  case it
  case nl
  case no
  case pt
  case ru
  case se
  case ud
  case zh
  
}
