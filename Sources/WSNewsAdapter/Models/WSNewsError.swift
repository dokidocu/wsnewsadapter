//
//  WSNewsError.swift
//  HLKit
//
//  Created by Henri LA on 26.10.19.
//

import Foundation

public enum WSNewsError: Error {

    /// No error
    case none
    /// Request will fail due to missing parameters
    case missingParameters(String)
    /// Invlalid URL -> nil was returned
    case invalidURL(String)
    /// Backend returns an error
    case backendError(WSNewsBackendError)
    /// Original error
    case dataTaskError(Error)
    /// No Data in response
    case noData
    /// Deserializing failed, parsing was not possible
    case parsingFailed

    public var code: Int {
        let result: Int

        switch self {
        case .none:
            result = 0
        case .missingParameters:
            result = 1
        case .invalidURL:
            result = 2
        case .backendError:
            result = 3
        case .dataTaskError:
            result = 4
        case .noData:
            result = 5
        case .parsingFailed:
            result = 6
        }

        return result
    }

    public var localizedDescription: String {
        let message: String

        switch self {
        case .none:
            message = ""
        case .missingParameters(let value):
            message = value
        case .invalidURL(let value):
            message = value
        case .backendError(let value):
            message = value.message
        case .dataTaskError(let value):
            message = value.localizedDescription
        case .noData:
            message = "No Data in response"
        case .parsingFailed:
            message = "Impossible to parse the data"
        }

        return message
    }

}
