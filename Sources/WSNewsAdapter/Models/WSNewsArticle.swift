//
//  Article.swift
//  HLNetKit_Example
//
//  Created by Henri LA on 18.10.19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import Foundation

public struct WSNewsArticle: Codable {
  
  /// The identifier id and a display name name for the source this article came from
  public let source: WSNewsSource?
  /// The author of the article
  public let author: String?
  /// The headline or title of the article.
  public let title: String?
  /// A description or snippet from the article.
  public let description: String?
  /// The direct URL to the article.
  public let url: String?
  /// The URL to a relevant image for the article.
  public let urlToImage: String?
  /// The date and time that the article was published, in UTC (+000)
  public let publishedAt: String?
  /// The unformatted content of the article, where available.
  /// This is truncated to 260 chars for Developer plan users.
  public let content: String?
  
}

extension WSNewsArticle: Equatable {
  
  public static func == (lhs: WSNewsArticle, rhs: WSNewsArticle) -> Bool {
    return lhs.source == rhs.source && lhs.url == rhs.url && lhs.author == rhs.author
  }
  
}
