//
//  Article.swift
//  HLNetKit_Example
//
//  Created by Henri LA on 18.10.19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import Foundation

/// The identifier id and a display name name for the source this article came from.
public struct WSNewsSource: Codable {
  
    public let id: String?
    public let name: String?
    public let description: String?
    public let url: String?
    public let category: WSNewsCategory?
    public let language: WSNewsLanguage?
    // Todo: Not working due to "in"...
    // public let country: WSNewsCountry?
}

extension WSNewsSource: Equatable {
  
    public static func == (lhs: WSNewsSource, rhs: WSNewsSource) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
  
}
