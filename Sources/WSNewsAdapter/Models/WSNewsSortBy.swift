//
//  WSNewsSortBy.swift
//  HLKit
//
//  Created by Henri LA on 27.10.19.
//

import Foundation

/// The order to sort the articles in.
public enum WSNewsSortBy: String, CaseIterable {
  
  case relevancy
  case popularity
  case publishedAt
  
}
